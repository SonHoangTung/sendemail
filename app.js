require('dotenv').config();
const environment = require('./environment');
const database = require('./config/database');
const server = require('./config/server');
const logger = require('./config/logger');

// Catch unhandling unexpected exceptions
process.on('uncaughtException', (error) => {
    logger.error(`uncaughtException ${error.message}`);
});

// Catch unhandling rejected promises
process.on('unhandledRejection', (reason) => {
    logger.error(`unhandledRejection ${reason}`);
});

const dbConfig = environment.getDatabaseConfig();
const db = database.init(dbConfig);

const serverConfig = environment.getServerConfig();
const appServer = server.init(serverConfig, db);

appServer.listen(process.env.PORT || serverConfig.port, function () {
    logger.info(`Server running at port ${serverConfig.port}`);
});