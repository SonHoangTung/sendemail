var dateFormat = require('dateformat');
var validation = require('../util/validation');

exports.sendEmailWithContent = function (req, res) {
    var email = req.body.email;
    var title = req.body.title;
    var content = req.body.content;
    var msgType = req.body.msgType;
    var date = dateFormat(new Date(), "yyyymmddHHMMss"); 

    req.getConnection(function (error, conn) {
        if (!conn) {
            res.status(404).send();
            return;
        }
        var QUERY = 'INSERT INTO M_PUSH_EMAIL (EMAIL, MSG_TP, MSG_TITLE, MSG_CONTENT, REQ_DT, SENDER_NM) VALUES(?,?,?,?,?,?)';
        conn.query(QUERY, [email, msgType, title, content, date, 'Infocity'], function (err, rows, fields) {
            if (err || validation.isEmptyJson(rows)) {
                console.log(err);
                res.status(404).json({ 'msg': err });
            } else {
                console.log("OK");
                res.status(200).send(rows);
            }
        });
    });
}