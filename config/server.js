'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const myConnection = require('express-myconnection');
const logger = require('./logger');
const mysql = require('mysql');
const expressValidator = require('express-validator');

const methodOverride = require('method-override');
const flash = require('express-flash');
const cookieParser = require('cookie-parser');
const session = require('express-session');

const email = require('../routes/email');

module.exports.init = function (configs, db) {
    const app = express();
    app.use(cors());
    app.use(express.static('public'));
    app.use(morgan('combined', { stream: logger.stream }));
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());

    var dbOptions = {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PWD,
        port: process.env.DB_PORT,
        database: process.env.DB_NAME,
        multipleStatements: true
    };

    app.use(myConnection(mysql, dbOptions, 'pool'));
    app.use(expressValidator());

    app.use(methodOverride(function (req, res) {
        if (req.body && typeof req.body === 'object' && '_method' in req.body) {
            var method = req.body._method;
            delete req.body._method;
            return method;
        }
    }));

    app.use(cookieParser('keyboard cat'));
    app.use(session({
        secret: 'keyboard cat',
        resave: false,
        saveUninitialized: true,
        cookie: {
            maxAge: 60000
        }
    }));
    app.use(flash());
    app.use('/email', email);
    return app;
};