const winston = require('winston');
const { Console } = require('winston/lib/winston/transports');

require('winston-daily-rotate-file');

const DATE_PATTERN = 'YYYY-MM-DD HH:mm:ss';

const transport = new (winston.transports.DailyRotateFile)({
  filename: `${__dirname}/../logs/application-%DATE%.log`,
  datePattern: 'DD-MM-YYYY',
  zippedArchive: true,
  maxSize: '20m',
  maxFiles: '30d',
});

const options = {
  file: {
    level: 'info',
    filename: `${__dirname}/../logs/app.log`,
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 1,
  },
  console: {
    level: 'debug',
    handleExceptions: true,
    json: false,
    colorize: true,
  },
};

const logger = winston.createLogger({
  transports: [
    new winston.transports.File(options.file),
    new winston.transports.Console(options.console),
    transport,
  ],
  format:
    winston.format.combine(
      winston.format.splat(),
      winston.format.simple(),
      winston.format.timestamp({
        format: DATE_PATTERN,
      }),
      winston.format.printf((info) => {
        if(info.stack) return `[${info.timestamp}] [${info.level}] ${info.stack}`;
        return `[${info.timestamp}] ${info.level}: ${info.message}`;
      })),
  exitOnError: false,
});

logger.stream = {
  write(message) {
    logger.info(message);
  },
};

module.exports = logger;