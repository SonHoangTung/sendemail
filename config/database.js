const mysql = require('mysql2/promise');

module.exports.init = function (configs) {

    return mysql.createPool({
        host: process.env.MYSQL_HOST || configs.host,
        port: process.env.MYSQL_PORT || configs.port,
        user: process.env.MYSQL_USER || configs.user,
        password: process.env.MYSQL_PASSWORD|| configs.password,
        connectionLimit: process.env.MYSQL_CONNECTION_LIMIT || configs.connectionLimit,
        database: configs.database,
        debug: configs.debug
    });
};

