var express = require('express');
var router = express.Router();
var email_controller = require('../controller/emailController');

router.post('/sendEmailWithContent', email_controller.sendEmailWithContent);
module.exports = router;